# IrishBusCLI

![GNU](https://badgen.net/badge/Licence/GNU/green)

**IrishBusCLI** is a CLI tool, allowing to know the next buses on a specific stop. 

```
$ irish-bus --help

USAGE:
    irish-bus [OPTIONS] [stopid]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -n, --number <number>    Number of results displayed [default: 4]

ARGS:
    <stopid>    ID of the bus stop [default: 246441]
```

ScreenShot :

![alt text](img/screenshot.png)

[**Release link**](https://gitlab.com/Boluge/IrishBusCLI/tags)
