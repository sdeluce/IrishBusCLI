#[macro_use]
extern crate structopt;
#[macro_use]
extern crate serde_derive;

extern crate colored;
extern crate reqwest;
extern crate serde;
extern crate serde_json;
extern crate url;

use reqwest::Error;
use structopt::StructOpt;
use url::Url;

use colored::*;

const URL: &'static str = "https://data.smartdublin.ie/cgi-bin/rtpi/realtimebusinformation";

#[derive(Debug, StructOpt)]
#[structopt(name = "irish-bus", about="\nYou will find the bus stop IDs either on the TFI website or directly on your bus stops.\n(Model Farm Rd: 246441 | Melbourne Rd: 241861 | CIT: 210231)")]
struct Opt {
    /// Option for number of results displayed
    #[structopt(
        help = "Number of results displayed", short = "n", long = "number", default_value = "4"
    )]
    number: i32,
    /// Stopid number, by default 241861 or 246441
    #[structopt(help = "ID of the bus stop")]
    stopid: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Bus {
    pub arrivaldatetime: String,
    pub duetime: String,
    pub departuredatetime: String,
    pub departureduetime: String,
    pub scheduledarrivaldatetime: String,
    pub scheduleddeparturedatetime: String,
    pub destination: String,
    pub destinationlocalized: String,
    pub origin: String,
    pub originlocalized: String,
    pub direction: String,
    pub operator: String,
    pub additionalinformation: String,
    pub lowfloorstatus: String,
    pub route: String,
    pub sourcetimestamp: String,
    pub monitored: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct AllResults {
    pub errorcode: String,
    pub errormessage: String,
    pub numberofresults: i64,
    pub stopid: String,
    pub timestamp: String,
    pub results: Vec<Bus>,
}

fn get_bus(stopid: String, number: i32) -> Result<AllResults, Box<Error>> {
    let url = Url::parse_with_params(
        URL,
        &[
            ("format", "json"),
            ("stopid", &stopid),
            ("maxresults", &number.to_string()),
        ],
    );

    let resp = reqwest::get(url.unwrap()).unwrap().text();
    let results: AllResults = serde_json::from_str(&resp.unwrap()).unwrap();
    Ok(results)
}

fn show_results(results: &AllResults) {
    print!("\n");
    println!("Stop ID:\t{}", results.stopid.yellow().bold());
    println!("Date & Time:\t{}", results.timestamp);
    print!("\n");
    for bus in &results.results {
        print!(
            "Bus {}-{}",
            bus.route.cyan().bold(),
            bus.destination.cyan().bold()
        );

        if bus.departureduetime != "Due" {
            let mut nb_minutes = bus.departureduetime.green().bold();
            let mut minutes;
            let time = bus.departureduetime.parse::<i32>().unwrap();

            if time < 2 {
                minutes = "minute";
            } else {
                minutes = "minutes";
            }

            if time < 5 {
                nb_minutes = bus.departureduetime.red().bold();
            } else if time < 10 && time >= 5 {
                nb_minutes = bus.departureduetime.yellow().bold();
            }
            print!(
                ", arrive {}, in {} {}.",
                bus.arrivaldatetime, nb_minutes, minutes
            );
        } else {
            print!(", at the bus stop.");
        }
        if bus.monitored == "true" {
            print!(" ({})", "Live".blue());
        }
        print!("\n");
    }
    if &results.results.len().to_string() == "0" {
        println!("{}\n", "Sorry, no results. Try it later.".red().bold());
    }
    print!("\n");
}

fn main() {
    let opt = Opt::from_args();
    let result = get_bus(opt.stopid, opt.number);
    show_results(&result.unwrap());
}
